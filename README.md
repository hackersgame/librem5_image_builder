Librem5 Image Builder
============
Turns a rootfs for arm64 into a bootable image for the Librem 5


Usage:
====
Download the latest "plain ​librem5 ​amber-phone ​image" from
https://arm01.puri.sm/job/Images/job/Image%20Build/
This provids librem5.img.xz decompress it in librem5_image_builder/original_image/ as librem5.img

-----------------
Put your arm64 root file system into root_fs
Most pinephone images can have their rootsfs extracted with the following:
sudo ./image_mount.sh path_to_some.img /mnt
sudo cp -arx /mnt/* ./root_fs
umount /mnt
-----------------
With librem5.img in  original_image and your root fs in place, run: 
./build.sh

-----------------
#Grow librem 5 image
dd if=/dev/zero bs=1M count=2048 >> librem5.img
sudo parted ./librem5.img
resize 2
7295MB (Should have been printed when you run "print". Parted: DISK ./librem5.img xMB)
quit
#mount with image_mount.sh and run resize2fs on p2
-----------------
license
-------
GPL3
By David Hamner
