#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

part="p2"
echo "Mounting $1 to $2"
image=$1
mount_point=$2

#setup loopback
losetup -Pf $image
device=$(losetup -a | sort | tail -n 1 | cut -d ":" -f1)

#mount
mount $device$part $mount_point

echo -e "To unmount: \numount $mount_point\nlosetup -D $image"
