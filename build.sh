#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

cp ./original_image/librem5.img ./output/
./image_mount.sh ./output/librem5.img /mnt

rm -r original_image/parts_to_keep 2>/dev/null
mkdir original_image/parts_to_keep

echo "Backing up stuff needed to boot"
#cp -a /mnt/etc/fstab ./original_image/parts_to_keep/fstab
#cp -ar /mnt/lib/modules ./original_image/parts_to_keep/
mv /mnt/usr/lib/modules /mnt/pureOS_modules
mv /mnt/etc /mnt/pureOS_etc

echo "Removing pureOS files"
rm -rf /mnt/debconf.set /mnt/lib /mnt/media /mnt/opt /mnt/root /mnt/sbin /mnt/sys /mnt/usr /mnt/dev /mnt/home /mnt/mnt /mnt/proc /mnt/run /mnt/srv /mnt/tmp /mnt/var /mnt/bin /mnt/etc


echo "Copying rootFS"
echo "If you see an error, you may need to grow the liberm.img (See README.md)"
cp -ar --preserve=links ./root_fs/* /mnt/

echo "Copying back boot files"
rm /mnt/etc/fstab 2> /dev/null
cp -a /mnt/pureOS_etc/fstab /mnt/etc/
cp -a /mnt/pureOS_modules/5* /mnt/lib/modules/

echo "Running MESA workaround"
echo "ETNA_MESA_DEBUG=no_supertile" >> /mnt/etc/environment

echo "Cleaning up..."
umount /mnt
losetup -D ./output/librem5.img

echo "Output file: ./output/librem5.img"
